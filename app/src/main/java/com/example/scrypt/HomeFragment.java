package com.example.scrypt;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.scrypt.databinding.FragmentHomeBinding;


public class HomeFragment extends Fragment {

    FragmentHomeBinding binding;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        binding.setFragment(this);
        return binding.getRoot();
    }

    public void encrypt(){
        navigateToCryptFragment(MainActivity.ENCRYPT);
    }

    public void decrypt(){
        navigateToCryptFragment(MainActivity.DECRYPT);
    }

    private void navigateToCryptFragment(int type){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, CryptFragment.newInstance(type));
        ft.addToBackStack(CryptFragment.class.getSimpleName());
        ft.commit();
    }

}
