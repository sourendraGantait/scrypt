package com.example.scrypt;

public class CryptUtils {

    public String getEncryptedText(String text) {
        StringBuilder result = new StringBuilder();
        if (text.contains("\\s")) {
            String[] wordsList = text.split("\\s");
            for (int i = 0; i < wordsList.length; i++) {
                if (i != 0) {
                    result.append(" 1");
                }
                result.append(wordsList[i]);
            }
        } else result.append(encrypt(text));
        return result.toString();
    }

    public String encrypt(String text) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (i != 0 && text.charAt(i - 1) == text.charAt(i)) {
                int count = Integer.parseInt(String.valueOf(result.charAt(result.length() - 1)));
                result.deleteCharAt(result.length() - 1);
                result.append(count + 1);
            } else
                result.append(text.charAt(i)).append("1");
        }
        return result.toString();
    }

    public String getDecryptedText(String text) {
        StringBuilder result = new StringBuilder();
        if (text.contains("\\s")) {
            String[] wordList = text.split("\\s");
            for (int i = 0; i < wordList.length; i++) {
                if (i != 0) {
                    String removedFirstOne = wordList[i].substring(1);
                    result.append(decrypt(removedFirstOne));
                } else
                    result.append(decrypt(wordList[i]));
            }
        } else
            result.append(decrypt(text));
        return result.toString();
    }

    public String decrypt(String text) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (i != 0 && Character.isDigit(text.charAt(i)) && Integer.parseInt(String.valueOf(text.charAt(i))) > 1 && i % 2 == 1) {
                int count = Integer.parseInt(String.valueOf(text.charAt(i)));
                result.deleteCharAt(result.length() - 1);
                for (int j = 0; j < count; j++) {
                    result.append(text.charAt(i - 1));
                }
            } else if (i % 2 == 0) {
                result.append(text.charAt(i));
            }
        }
        return result.toString();
    }
}
