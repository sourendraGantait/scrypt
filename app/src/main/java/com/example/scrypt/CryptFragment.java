package com.example.scrypt;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.scrypt.databinding.FragmentCryptBinding;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CryptFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CryptFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    FragmentCryptBinding binding;

    // TODO: Rename and change types of parameters
    private int type;

    public CryptFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param type Parameter 1.
     * @return A new instance of fragment CryptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CryptFragment newInstance(int type) {
        CryptFragment fragment = new CryptFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setTitle();
        showBackButton();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_crypt, container, false);
        binding.setEncrypt(type == MainActivity.ENCRYPT);
        binding.setFragment(this);
        return binding.getRoot();
    }

    private void setTitle(){
        switch (type) {
            case MainActivity.ENCRYPT:
                getActivity().setTitle(getResources().getString(R.string.encrypt));
                break;
            case MainActivity.DECRYPT:
                getActivity().setTitle(getResources().getString(R.string.decrypt));
                break;
        }
    }

    private void showBackButton() {
        ActionBar actionbar = ((MainActivity) getActivity()).getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    private void hideBackButton() {
        ActionBar actionbar = ((MainActivity) getActivity()).getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
    }

    public void encryptText(String text) {
        if (text != null && text.trim().length() > 0) {
            String result = new CryptUtils().getEncryptedText(text);
            binding.setOutput(result);
        } else {
            showToast(getContext().getResources().getString(R.string.enter_something));
        }
    }

    public void decryptText(String text) {
        if (text != null && text.trim().length() > 0) {
            String result = new CryptUtils().getDecryptedText(text);
            binding.setOutput(result);
        } else {
            showToast(getContext().getResources().getString(R.string.enter_something));
        }
    }

    private void showToast(String message) {
        if (message != null && message.trim().length() > 0) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDetach() {
        getActivity().setTitle(getResources().getString(R.string.app_name));
        hideBackButton();
        super.onDetach();
    }
}
